import * as styled from 'styled-components'

declare module 'styled-components' {
  // eslint-disable-next-line @typescript-eslint/ban-types
  export type Styled<P extends object> = styled.StyledComponent<any, {}, P>
}
