import { useParams } from 'react-router'

import { CommonTemplate } from 'features/common'
import { CategoryFilters } from 'features/category-filters'

interface ParamTypes {
  categoryId: string
}

export const CategoryPage: React.FC = () => {
  const params = useParams<ParamTypes>()

  return (
    <CommonTemplate>
      <CategoryFilters {...{ categoryId: params.categoryId }} />
    </CommonTemplate>
  )
}
