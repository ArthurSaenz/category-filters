export const paths = {
  home: () => '/',
  login: () => '/login',
  category: (categoryId = ':categoryId') => `/category/${categoryId}`,
  /**
   * @example
   * user: (username = ':username') => `/@${username}`,
   */
}
