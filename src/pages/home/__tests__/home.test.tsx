import { render, RenderResult } from '@testing-library/react'

import { ThemeProvider } from '@arters/ui/themes'

import { HomePage } from '../index'

let documentBody: RenderResult

// TODO: need to general Wrap
describe('<HomePage />', () => {
  beforeEach(() => {
    documentBody = render(
      <ThemeProvider>
        <HomePage />
      </ThemeProvider>,
    )
  })
  it('matches snapshot', () => {
    const { baseElement } = documentBody
    expect(baseElement).toMatchSnapshot()
  })
})
