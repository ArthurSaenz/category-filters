import { paths } from './paths'
import { HomePage } from './home'
import { CategoryPage } from './category'
import { Error404Page } from './error404'

export const ROUTES = [
  {
    path: paths.home(),
    exact: true,
    component: HomePage,
  },
  {
    path: paths.category(),
    exact: true,
    component: CategoryPage,
  },
  {
    path: '*',
    component: Error404Page,
  },
]
