import { useTranslation } from 'react-i18next'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import ListAltIcon from '@material-ui/icons/ListAlt'
import HomeIcon from '@material-ui/icons/Home'

import { historyPush } from 'features/navigation'

import { paths } from 'pages/paths'

interface Params {
  t: (arg: string) => string
  indexOfProfile: string
}

const getMenuList = ({ t, indexOfProfile }: Params) => [
  {
    icon: <HomeIcon fontSize="small" />,
    label: t('home_label'),
    handler: historyPush.prepend(paths.home),
  },
  {
    icon: <ListAltIcon fontSize="small" />,
    label: t('filters_label'),
    handler: historyPush.prepend(() => paths.category(indexOfProfile)),
  },
]

export const Sidebar = () => {
  const [t] = useTranslation(['common'])

  const indexOfProfile = '5'

  return (
    <MenuList>
      {getMenuList({ t, indexOfProfile }).map((item, index) => (
        <MenuItem key={index} onClick={() => item.handler(undefined)}>
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemText primary={item.label} />
        </MenuItem>
      ))}
    </MenuList>
  )
}
