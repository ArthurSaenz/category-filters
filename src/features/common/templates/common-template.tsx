import { GenericTemplate } from '@arters/ui/templates'

import { Header, Sidebar } from '../organisms'

export const CommonTemplate: React.FC = ({ children }) => {
  return (
    <GenericTemplate
      {...{
        header: <Header />,
        sidebar: <Sidebar />,
        footer: <div>footer</div>,
      }}
    >
      {children}
    </GenericTemplate>
  )
}
