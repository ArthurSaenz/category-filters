/* eslint-disable react/prop-types */
import { useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import Checkbox from '@material-ui/core/Checkbox'
import IconButton from '@material-ui/core/IconButton'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'

import { CategoryGroupItemType } from 'api/category'

import { Box } from '@arters/ui/atoms'

import {
  toggleItemCategory,
  selectAllCategory,
  resetCategory,
} from '../models/category.model'

interface CategoryContentProps {
  nameGroup: string
  isResetAvailable: boolean
  categorySelectedIds: string[]
  itemList: CategoryGroupItemType[]
}

export const CategoryContent: React.FC<CategoryContentProps> = ({
  nameGroup,
  isResetAvailable,
  categorySelectedIds,
  itemList,
}) => {
  const [t] = useTranslation(['common'])

  const handleSelectAll = useCallback(() => selectAllCategory(nameGroup), [
    nameGroup,
  ])

  const handleResetCategory = useCallback(() => resetCategory(nameGroup), [
    nameGroup,
  ])

  return (
    <Box flex={1} gap={1}>
      <Box display="flex" gap={1} justifyContent="flex-end">
        {isResetAvailable && (
          <Button size="small" variant="outlined" onClick={handleResetCategory}>
            {t('reset_label1')}
          </Button>
        )}
        <Button size="small" variant="outlined" onClick={handleSelectAll}>
          {t('selectAll_label')}
        </Button>
      </Box>
      <List>
        {itemList.map((item) => (
          <ListItem
            key={item.id}
            role={undefined}
            dense
            button
            onClick={() => toggleItemCategory([nameGroup, item])}
          >
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={categorySelectedIds.includes(item.id)}
                tabIndex={-1}
                disableRipple
                inputProps={{ 'aria-labelledby': item.id }}
              />
            </ListItemIcon>
            <ListItemText
              id={item.id}
              primary={item.title}
              secondary="some info"
            />
            <ListItemSecondaryAction>
              <IconButton edge="end" aria-label="menu">
                <MoreHorizIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </Box>
  )
}
