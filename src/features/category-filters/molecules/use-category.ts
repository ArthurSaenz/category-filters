import { useStoreMap } from 'effector-react'

import { $mainCategoryData, $categorySelected } from '../models/category.model'

export const useCategory = (nameGroup: string) => {
  const itemList = useStoreMap({
    store: $mainCategoryData,
    keys: [nameGroup],
    fn(state, [name]) {
      if (name in state) return Object.values(state[name])
      return []
    },
  })

  const categorySelectedIds = useStoreMap({
    store: $categorySelected,
    keys: [nameGroup],
    fn(state, [name]) {
      if (name in state) return state[name].map((element) => element.id)
      return []
    },
  })

  const categorySelectedCount = categorySelectedIds.length || 0

  return {
    itemList,
    categorySelectedIds,
    categorySelectedCount,
  }
}
