/* eslint-disable react/jsx-props-no-spreading */
import Accordion from '@material-ui/core/Accordion'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { CategoryContent } from './category-content'
import { useCategory } from './use-category'

interface CategoryItem {
  nameGroup: string
}

export const CategoryItem: React.FC<CategoryItem> = ({ nameGroup }) => {
  const { categorySelectedIds, categorySelectedCount, itemList } = useCategory(
    nameGroup,
  )

  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-label="Expand"
        aria-controls="additional-actions3-content"
        id={`${nameGroup}_accordion_group`}
      >
        <Typography variant="h6">
          {`${nameGroup}  ${
            categorySelectedCount ? `(${categorySelectedCount})` : ''
          }`}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <CategoryContent
          {...{
            nameGroup,
            categorySelectedIds,
            isResetAvailable: categorySelectedCount >= 1,
            itemList,
          }}
        />
      </AccordionDetails>
    </Accordion>
  )
}
