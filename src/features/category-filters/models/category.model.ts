import {
  createStore,
  combine,
  createEvent,
  attach,
  createEffect,
} from 'effector-root'
import { createGate } from 'effector-react'

import * as categoryApi from 'api/category'
import { CategoryDataType, CategoryGroupItemType } from 'api/category'

import { createStepsModel } from 'lib/effector-helpers'

import { mockData } from './mock-data'

export interface CategoryDataStateType {
  [key: string]: {
    [key: string]: CategoryGroupItemType
  }
}

export interface CategorySelectedStateType {
  [key: string]: CategoryGroupItemType[]
}
// START: definition events
export const removeItemCategory = createEvent<[string, string]>()
export const toggleItemCategory = createEvent<[string, CategoryGroupItemType]>()
export const selectAll = createEvent()
export const selectAllCategory = createEvent<string>()
export const resetCategory = createEvent<string>()
export const resetAllCategory = createEvent()
export const triggerConfirmCategory = createEvent()

// END: definition events

// START: definition async fetch

// export const loadCategoryFx = attach({
//   mapParams: (categoryId) => categoryId,
//   effect: categoryApi.loadCategoryFx,
// })

export const loadCategoryFx = createEffect<undefined, CategoryDataType>({
  handler: () =>
    new Promise<CategoryDataType>((rs) => setTimeout(() => rs(mockData), 500)),
})

// END: definition async fetch

export const CategoryFiltersGate = createGate()

export const categoryStepsModel = createStepsModel()

export const $mainCategoryData = createStore<CategoryDataStateType>({})
export const $mainCategoryDataList = $mainCategoryData.map((value) =>
  Object.keys(value),
)

export const $categorySelected = createStore<CategorySelectedStateType>({})
export const $categorySelectedList = $categorySelected.map((state) =>
  Object.entries(state),
)

export const $isValid = $categorySelectedList.map((state) =>
  Object.values(state).some(([_, item]) => item.length >= 1),
)

// START: mobile view show more logic
export const SEPARATE_COUNT = 5

const $currentPage = createStore<number>(1)

export const handleShowMore = createEvent()

$currentPage.on(handleShowMore, (state) => state + 1)

export const $mainCategoryDataListActive = combine(
  $currentPage,
  $mainCategoryDataList,
  (currentPage, categoryList) =>
    categoryList.slice(0, SEPARATE_COUNT * currentPage),
)

export const $isNeedSeparation = $mainCategoryDataList.map(
  (list) => list.length > SEPARATE_COUNT,
)

export const $isNeedShowMore = combine(
  $mainCategoryDataListActive,
  $mainCategoryDataList,
  (a, b) => a.length !== b.length,
)
// END: mobile view show more logic
