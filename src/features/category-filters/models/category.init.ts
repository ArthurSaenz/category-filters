import * as R from 'ramda'
import { sample, forward } from 'effector-root'

import { toggleList } from 'lib/toggle-list'

import {
  $mainCategoryData,
  $categorySelected,
  CategoryFiltersGate,
  toggleItemCategory,
  removeItemCategory,
  selectAllCategory,
  selectAll,
  resetCategory,
  resetAllCategory,
  loadCategoryFx,
  categoryStepsModel,
} from './category.model'

forward({
  from: CategoryFiltersGate.open,
  to: loadCategoryFx,
})

$mainCategoryData
  .on(loadCategoryFx.done, (_, { result }) =>
    Object.entries(result).reduce(
      (acc, [groupName, categoryList]) => ({
        ...acc,
        [groupName]: R.indexBy(R.prop('id'), categoryList),
      }),
      {},
    ),
  )
  .reset(CategoryFiltersGate.close)

const launchSelectAllCategory = sample({
  clock: selectAllCategory,
  source: $mainCategoryData,
  fn: (state, categoryGroupName) => ({ state, categoryGroupName }),
})

const launchSelectAll = sample({
  clock: selectAll,
  source: $mainCategoryData,
  fn: (mainCategoryData) => {
    const initValue = {}

    for (const [nameGroup, items] of Object.entries(mainCategoryData)) {
      initValue[nameGroup] = Object.values(items)
    }

    return initValue
  },
})

sample({
  source: launchSelectAll,
  target: categoryStepsModel.nextStep,
})

$categorySelected
  .on(loadCategoryFx.done, (_, { result }) =>
    Object.keys(result).reduce((acc, item) => ({ ...acc, [item]: [] }), {}),
  )
  .on(launchSelectAllCategory, (state, payload) => ({
    ...state,
    [payload.categoryGroupName]: Object.values(
      payload.state[payload.categoryGroupName],
    ),
  }))
  .on(resetAllCategory, (state) =>
    Object.keys(state).reduce((acc, key) => ({ ...acc, [key]: [] }), {}),
  )
  .on(launchSelectAll, (_, payload) => payload)
  .on(removeItemCategory, (state, [categoryGroupName, itemId]) => {
    if (!(categoryGroupName in state)) return state
    return {
      ...state,
      [categoryGroupName]: state[categoryGroupName].filter(
        (element) => element.id !== itemId,
      ),
    }
  })
  .on(toggleItemCategory, (state, [nameGroup, value]) => ({
    ...state,
    [nameGroup]: toggleList(value, state[nameGroup]),
  }))
  .on(resetCategory, (state, categoryGroupName) => {
    if (!(categoryGroupName in state)) return state
    return {
      ...state,
      [categoryGroupName]: [],
    }
  })

  .reset(CategoryFiltersGate.close)
