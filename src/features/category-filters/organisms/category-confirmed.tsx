import styled from 'styled-components'
import Chip from '@material-ui/core/Chip'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { useTranslation } from 'react-i18next'
import { useList } from 'effector-react'
import Divider from '@material-ui/core/Divider'
import VerifiedUserOutlinedIcon from '@material-ui/icons/VerifiedUserOutlined'

import { Box } from '@arters/ui/atoms'

import {
  resetCategory,
  removeItemCategory,
  resetAllCategory,
  $categorySelectedList,
} from '../models/category.model'

export const CategoryConfirmed = () => {
  const [t] = useTranslation(['common'])

  return (
    <Box pt={1} gap={2}>
      <Box
        display="flex"
        gap={1}
        px={1}
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography variant="h6">{t('applied_filters')}</Typography>
        <Button
          size="small"
          color="secondary"
          onClick={() => resetAllCategory()}
        >
          {t('remove_all_label')}
        </Button>
      </Box>
      <Box gap={3}>
        {useList($categorySelectedList, ([nameGroup, selectedList], index) => {
          if (selectedList.length === 0) return null

          return (
            <div>
              {index !== 0 && <Divider variant="middle" />}
              <GridTag key={nameGroup}>
                <Box gap={1} display="flex" pr={1.5} pt={0.5}>
                  <VerifiedUserOutlinedIcon />
                  <Typography variant="subtitle2">{nameGroup}</Typography>
                </Box>
                <WrapChip>
                  {selectedList.map(({ id, title }) => (
                    <Chip
                      key={title}
                      label={title}
                      onDelete={() => removeItemCategory([nameGroup, id])}
                      variant="outlined"
                    />
                  ))}

                  {selectedList.length > 1 && (
                    <Chip
                      key="reset"
                      color="secondary"
                      onDelete={() => resetCategory(nameGroup)}
                      label="Reset all"
                      variant="outlined"
                    />
                  )}
                </WrapChip>
              </GridTag>
            </div>
          )
        })}
      </Box>
    </Box>
  )
}

const WrapChip = styled.div`
  display: flex;
  flex-wrap: wrap;

  > * {
    margin: 0 10px 10px 0;
  }

  ${(p) => p.theme.breakpoints.down('md')} {
    display: contents;
  }
`

const GridTag = styled.div`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: 200px auto;
  padding-top: 15px;

  ${(p) => p.theme.breakpoints.down('md')} {
    display: flex;
    flex-wrap: wrap;
    grid-gap: 0;
  }
`
