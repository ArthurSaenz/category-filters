/* eslint-disable react-hooks/rules-of-hooks */
import { useStore, useList } from 'effector-react'
import styled from 'styled-components'
import Fab from '@material-ui/core/Fab'
import Button from '@material-ui/core/Button'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { useTranslation } from 'react-i18next'
import SelectAllIcon from '@material-ui/icons/SelectAll'

import { Container } from '@arters/ui/templates'
import { Box } from '@arters/ui/atoms'

import {
  $mainCategoryDataList,
  handleShowMore,
  loadCategoryFx,
  $isNeedSeparation,
  $isNeedShowMore,
  $mainCategoryDataListActive,
  selectAll,
} from '../models/category.model'
import { CategoryItem } from '../molecules'

export const CategoryList: React.FC = () => {
  const isTabletOrMobile = useMediaQuery('(max-width: 1224px)')
  const isLoading = useStore(loadCategoryFx.pending)

  if (isLoading) return null

  return isTabletOrMobile ? <CategoryMobileItems /> : <CategoryDefaultItems />
}

const CategoryDefaultItems: React.FC = () => {
  const [t] = useTranslation(['common'])

  return (
    <Box gap={5}>
      <Box display="flex" justifyContent="flex-end">
        <Fab
          variant="extended"
          onClick={() => selectAll()}
          color="primary"
          size="medium"
          aria-label="add"
        >
          <StyledSelectAllIcon />
          {t('accept_all_label')}
        </Fab>
      </Box>
      <Container>
        {useList($mainCategoryDataList, (nameGroup) => (
          <CategoryItem {...{ nameGroup }} />
        ))}
      </Container>
    </Box>
  )
}

const StyledSelectAllIcon = styled(SelectAllIcon)`
  &.MuiSvgIcon-root {
    margin-right: 1rem;
  }
`

// Mobile Comp
const CategoryMobileItems: React.FC = () => {
  const [t] = useTranslation(['common'])

  const isNeedSeparation = useStore($isNeedSeparation)
  const isNeedShowMore = useStore($isNeedShowMore)

  return isNeedSeparation ? (
    <Box gap={3}>
      {useList($mainCategoryDataListActive, (name) => (
        <CategoryItem nameGroup={name} />
      ))}
      {isNeedShowMore ? (
        <Box display="flex" justifyContent="center">
          <Button color="primary" onClick={() => handleShowMore()}>
            {t('show_more_label')}
          </Button>
        </Box>
      ) : null}
    </Box>
  ) : (
    <CategoryDefaultItems />
  )
}
