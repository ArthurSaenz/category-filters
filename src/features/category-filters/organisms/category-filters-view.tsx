/* eslint-disable react/jsx-key */
import React from 'react'
import { useGate, useStore } from 'effector-react'
import styled from 'styled-components'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { useTheme } from '@material-ui/core/styles'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'

import { StepList } from 'lib/effector-helpers'

import { LoadingContainer } from '@arters/ui/molecules'

import {
  CategoryFiltersGate,
  categoryStepsModel,
  loadCategoryFx,
} from '../models/category.model'
import { Footer } from '../atoms'

import { CategoryList } from './category-list'
import { CategoryConfirmed } from './category-confirmed'

interface CategoryFiltersProps {
  categoryId: string
}

export const CategoryFilters: React.FC<CategoryFiltersProps> = ({
  categoryId,
}) => {
  useGate(CategoryFiltersGate, { categoryId })

  const loading = useStore(loadCategoryFx.pending)

  return (
    <>
      <StyledGrid>
        <div>
          <SidebarBlock />
        </div>
        <div>
          <MainBlock />
        </div>
      </StyledGrid>
      <LoadingContainer {...{ isLoading: loading }} />
    </>
  )
}

const MainBlock = () => {
  return (
    <>
      <StepList {...{ model: categoryStepsModel, forceRender: true }}>
        <WrapStep>
          <CategoryList />
        </WrapStep>
        <WrapStep>
          <CategoryConfirmed />
        </WrapStep>
      </StepList>
      <Footer />
    </>
  )
}

const WrapStep = styled.div<{ isActive?: boolean }>`
  display: ${(p) => (p.isActive ? 'block' : 'none')};
`

const STEPS_LABELS = ['initial_label', 'confirm_label']

const SidebarBlock = () => {
  const theme = useTheme()
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'))

  const currentIndex = useStore(categoryStepsModel.$currentIndex)

  return (
    <Stepper
      {...{
        activeStep: currentIndex,
        orientation: isMobile ? 'horizontal' : 'vertical',
      }}
    >
      {STEPS_LABELS.map((label) => (
        <Step {...{ key: label, completed: false }}>
          <StepLabel {...{ optional: undefined }}>{label}</StepLabel>
        </Step>
      ))}
    </Stepper>
  )
}

const StyledGrid = styled.div`
  display: grid;
  grid-gap: 32px;
  grid-template-rows: auto;
  grid-template-columns: 25% auto;
  min-height: 100%;
  margin: 0 2rem;

  > div {
    padding-top: 2rem;
  }

  > div:first-child {
    border-right: 1px solid #e5e5ea;
  }

  ${(p) => p.theme.breakpoints.down('sm')} {
    grid-template-columns: 1fr;
    min-height: auto;

    > div:first-child {
      border-right: none;
    }
  }
`
