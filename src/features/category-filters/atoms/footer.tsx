import { useStore } from 'effector-react'
import { useTranslation } from 'react-i18next'
import Button from '@material-ui/core/Button'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'

import { Box } from '@arters/ui/atoms'
import {
  categoryStepsModel,
  $isValid,
  triggerConfirmCategory,
} from '../models/category.model'

export const Footer: React.FC = () => {
  const [t] = useTranslation(['common'])

  const isValid = useStore($isValid)
  const isFirstStep = useStore(categoryStepsModel.$isFirst)
  const isLastStep = useStore(categoryStepsModel.$isLast)

  return (
    <Box display="flex" my={5} gap={1}>
      {!isFirstStep && (
        <Button
          variant="contained"
          startIcon={<ArrowBackIosIcon />}
          onClick={() => categoryStepsModel.prevStep()}
        >
          {t('back_link')}
        </Button>
      )}
      {isLastStep && (
        <Button
          variant="contained"
          color="primary"
          onClick={() => triggerConfirmCategory()}
        >
          {t('confirm_label')}
        </Button>
      )}
      {!isLastStep && (
        <Button
          variant="outlined"
          endIcon={<ArrowForwardIosIcon />}
          disabled={!isValid}
          onClick={() => categoryStepsModel.nextStep()}
        >
          {t('next_link')}
        </Button>
      )}
    </Box>
  )
}
