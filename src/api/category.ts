import * as typed from 'typed-contracts'

import { createResource } from './request'

const TCategoryGroupItem = typed.object({
  id: typed.string,
  title: typed.string,
})

const TCategoryData = typed.objectOf(typed.array(TCategoryGroupItem))

export type CategoryGroupItemType = typed.Get<typeof TCategoryGroupItem>

export type CategoryDataType = typed.Get<typeof TCategoryData>

const TCategoryGetSuccess = typed.object({
  data: TCategoryData,
})

export const loadCategoryFx = createResource({
  name: 'loadCategoryFx',
  contractDone: TCategoryGetSuccess,
  contractFail: typed.nul,
  mapParams: (categoryId) => ({
    path: `/category/get/${categoryId}`,
    method: 'GET',
  }),
})

const TCategorySubmitSucceeded = typed.object({
  firstName: typed.string,
  lastName: typed.string,
})

const TCategorySubmitFailed = typed.object({
  error: typed.union('invalid_credentials', 'invalid_form', 'invalid_payload'),
})

interface CategoryData {
  email: string
  password: string
}

export const submitCategoryFx = createResource({
  name: 'submitCategoryFx',
  contractDone: TCategorySubmitSucceeded,
  contractFail: TCategorySubmitFailed,
  mapParams: (categoryData: CategoryData) => ({
    path: '/category/submit',
    method: 'POST',
    body: categoryData,
  }),
})
