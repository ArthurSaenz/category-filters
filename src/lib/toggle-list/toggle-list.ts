import * as R from 'ramda'

export const toggleList = R.curry((value, list) =>
  R.ifElse(R.contains(value), R.without([value]), R.append(value))(list),
)

// Example:
// toggleListItem('apple')(['apple', 'mellow', 'cherry']) => ['mellow', 'cherry']
// toggleListItem('blackberry')(['mellow', 'cherry']) => ['mellow', 'cherry', 'blackberry']
