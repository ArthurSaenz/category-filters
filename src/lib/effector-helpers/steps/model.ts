/* eslint-disable no-shadow */
import { createApi, createStore, createEvent, sample, guard } from 'effector'
import { createGate } from 'effector-react'

interface StateType {
  current: number
  limit: number
}

interface MetaProps {
  sid?: string | undefined
  name?: string | undefined
}

export const createStepsModel = (
  _?: unknown,
  { sid, name }: MetaProps = {},
) => {
  const GateStepsModel = createGate<StateType>()

  const $steps = createStore<StateType>(
    {
      current: 1,
      limit: 1,
    },
    {
      name: `${name}.$steps`,
      sid: `initModelVal${sid}`,
    },
  )

  $steps.on(GateStepsModel.open, (_, { limit, current }) => ({
    current,
    limit,
  }))

  $steps.on(GateStepsModel.state.updates, (state, props) => {
    if (Object.entries(props).length === 0) return

    return {
      limit: props.limit,
      current: state.current > props.limit ? props.limit : state.current,
    }
  })

  const $current = $steps.map((state) => state.current)
  const $currentIndex = $steps.map((state) => state.current - 1)
  const $limit = $steps.map((state) => state.limit)
  const $isFirst = $steps.map((state) => state.current === 1)
  const $isLast = $steps.map((state) => state.current === state.limit)

  const changeStep = createEvent<string | number>()

  const mainStepsApi = createApi($steps, {
    changeStep(state, newStep) {
      if (newStep > state.limit) return state
      return { ...state, current: newStep }
    },
    prevStep(state) {
      if (state.current - 1 < 1) return state
      return { ...state, current: state.current - 1 }
    },
    nextStep(state) {
      if (state.current + 1 > state.limit) return state
      return { ...state, current: state.current + 1 }
    },
    reset(state) {
      return { ...state, current: 1 }
    },
  })

  interface GuardProps {
    current: number
    newIndex: number
  }

  guard<GuardProps>({
    source: sample({
      clock: changeStep,
      source: $current,
      fn: (current, newIndex) => {
        return { current, newIndex: newIndex ? Number(newIndex) + 1 : 1 }
      },
    }),
    filter: (payload) => payload.current !== payload.newIndex,
    target: mainStepsApi.changeStep.prepend<GuardProps>(
      (value) => value.newIndex,
    ),
  })

  return {
    $currentIndex,
    $current,
    $limit,
    $isFirst,
    $isLast,
    changeStep,
    reset: mainStepsApi.reset,
    nextStep: mainStepsApi.nextStep,
    prevStep: mainStepsApi.prevStep,
    Gate: GateStepsModel,
  }
}
