# Steps component

```js
import { Steps, createStepsModel } from '...'

export const ordersModel = createStepsModel()

export const CustomOrdersSteps = () => (
  <Steps model={ordersModel} forceRender>
    <CustomStep1 />
    <CustomStep2 />
    <CustomStep3 />
    <CustomStep4 />
  </Steps>
)
```

## Public stores

| Name                 | Default | Description                                             |
| -------------------- | ------- | ------------------------------------------------------- |
| `model.$currentStep` | 1       | Base directory to use instead of the default            |
| `model.$limit`       | null    | After first render will be count of children <Steps/>   |
| `model.$isFirst`     | true    | Valid file extensions used to find files in directories |
| `model.$isLast`      | false   | Valid file extensions used to find files in directories |

## Public events

| Name               | Params | Description                                                          |
| ------------------ | ------ | -------------------------------------------------------------------- |
| `model.changeStep` | number | Set number of step, the first is 1 (not zero).                       |
| `model.nextStep`   | -      | Action for next steps, if isLast is true, action will not be avaible |
| `model.prevStep`   | -      | Valid file extensions used to find files in directories              |
