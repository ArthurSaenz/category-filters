/* eslint-disable react/prop-types */
import React from 'react'
import { useStore, useGate } from 'effector-react'

interface StepListProps {
  initCurrent?: number
  forceRender?: boolean
  model: any
}

export const StepList: React.FC<StepListProps> = ({
  children,
  model,
  initCurrent,
  forceRender,
}) => {
  const currentStep: number = useStore(model.$currentIndex)

  const childrenArray = React.Children.toArray(children)

  let limit = 1

  if (childrenArray.length !== 0) {
    limit = childrenArray.length
  }

  useGate(model.Gate, {
    limit,
    current: initCurrent || 1,
  })

  if (forceRender) {
    return React.Children.map(children, (child: any, index) => {
      const activeId = (currentStep || 0) === index
      return React.cloneElement(child, {
        isActive: activeId,
      })
    })
  }

  return <>{childrenArray[currentStep || 0]}</>
}
