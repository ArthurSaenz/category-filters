import { RouteConfig } from 'react-router-config'

export interface RouteGuardConfig extends RouteConfig {
  guards?: Array<<T>(arg0: RouteConfig, arg1: T) => RouteConfig>
}
