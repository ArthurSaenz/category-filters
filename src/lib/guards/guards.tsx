import { Redirect } from 'react-router-dom'
import { RouteConfig } from 'react-router-config'

export const onlyAnonym = ({
  redirectUser,
  redirectApprovedUser,
  redirectUser2fa,
}: Record<string, string>) => <
  T extends { isAuth: boolean; isTwofaEnabled: boolean; isApprovedKYC: boolean }
>(
  route: RouteConfig,
  { isAuth, isTwofaEnabled, isApprovedKYC }: T,
) =>
  isAuth
    ? {
        ...route,
        component: () => {
          let path =
            redirectApprovedUser && isApprovedKYC
              ? redirectApprovedUser
              : redirectUser
          path = isTwofaEnabled ? redirectUser2fa : path

          return <Redirect to={path} />
        },
      }
    : route

export const onlyUser = ({ redirectAnon }: { redirectAnon: string }) => <
  T extends { isAuth: boolean }
>(
  route: RouteConfig,
  context: T,
) =>
  context.isAuth
    ? route
    : { ...route, component: () => <Redirect to={redirectAnon} /> }

export const checkRoles = (allow: string[]) => <T extends { roles: string[] }>(
  route: RouteConfig,
  context: T,
) => (allow.find((role) => context.roles.includes(role)) ? route : null)

export const checkFeatures = (featureFlag: string) => <
  T extends { features: Record<string, boolean> }
>(
  route: RouteConfig,
  context: T,
) => {
  return context.features[featureFlag] ? route : null
}

export const onlyApprovedUser = ({ redirectUser }: Record<string, string>) => <
  T extends { isApproved: boolean }
>(
  route: RouteConfig,
  context: T,
) =>
  context.isApproved
    ? route
    : { ...route, component: () => <Redirect to={redirectUser} /> }
