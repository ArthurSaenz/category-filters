/* eslint-disable @typescript-eslint/no-unnecessary-condition */
import { RouteConfig } from 'react-router-config'

import { RouteGuardConfig } from './types'

export function compileGuards<T>(routes: Array<RouteGuardConfig>, context: T) {
  return routes
    .map(
      (route): RouteConfig =>
        route.guards
          ? route.guards.reduce(
              (currentRoute, guardItem): any =>
                currentRoute ? guardItem<T>(currentRoute, context) : undefined,
              route,
            )
          : route,
    )
    .filter(Boolean)
    .map((route) => ({
      path: route.path,
      component: route.component,
      exact: true,
    }))
}
