/* eslint-disable global-require */
/* eslint-disable no-underscore-dangle */
import { Domain, root } from 'effector-root'
import { Event, Store, Effect } from 'effector'

declare global {
  interface Window {
    // add you custom properties and methods
    _app: {
      events: {
        [key: string]: Event<any>
      }
      stores: {
        [key: string]: Store<any>
      }
      effects: {
        [key: string]: Effect<any, any, any>
      }
    }
  }
}

const bindDomain = (domain: Domain) => {
  window._app = {
    events: {},
    stores: {},
    effects: {},
  }
  domain.onCreateEvent((event) => {
    const name = event.compositeName.shortName

    window._app.events[name] = event
  })

  domain.onCreateStore((store) => {
    const name = store.compositeName.shortName

    window._app.stores[name] = store
  })

  domain.onCreateEffect((effect) => {
    const name = effect.compositeName.shortName

    window._app.effects[name] = effect
  })
}

export const initLogger = () => {
  if (process.env.EFFECTOR_MAIN_LOGGER !== 'false') {
    const { attachLogger } = require('effector-logger/attach')

    // For only redux devtools enabled
    // attachLogger(root, { console: 'disabled', inspector: 'disabled' })

    attachLogger(root)

    // const createInspector = require('effector-inspector')

    bindDomain(root)
    // createInspector()

    console.log('Effector units (in window._app):', window._app)
  }
}
