import * as R from 'ramda'

export const renameKey = R.curry(
  (oldKey: string, newKey: string, object: Record<string, unknown>) =>
    R.assoc(newKey, R.prop(oldKey, object), R.dissoc(oldKey, object)),
)

// Example
// renameKey('name', 'title', { name: 'First', desc: 'other' }) => { title: 'First', desc: 'other' }
