import {
  createApi,
  createStore,
  is,
  sample,
  createEvent,
  combine,
  Effect,
  Store,
} from 'effector'
import { debounce } from 'patronum/debounce'

interface IPaginationProps {
  defaultLimit: number
  effect?: Effect<any, any, any>
  defaultParams: { [key: string]: string | string[] | Store<string> }
}

interface MetaProps {
  sid?: string
  name?: string
}

export const createPaginationModel = (
  { defaultLimit = 9, effect, defaultParams = {} }: IPaginationProps,
  { sid, name }: MetaProps = {},
) => {
  if (!effect && !is.effect(effect)) {
    throw new TypeError('Argument must have effect property')
  }

  const reset = createEvent()
  const resetParams = createEvent()

  const defaultParamsOrigin = Object.entries(defaultParams).reduce(
    (acc, [key, value]) => ({
      ...acc,
      [key]: getInitValueArg(value),
    }),
    {},
  )

  const initialState = {
    limit: defaultLimit,
    page: 1,
    totalPages: 1,
    total: 0,
    isLast: false,
    searchValueOrigin: '',
    searchValueDebounced: '',
    params: defaultParamsOrigin,
  }

  const $pagination = createStore(initialState, {
    name: `${name}.$pagination`,
    sid: `modelValue${sid}`,
  })

  const $currentPage = $pagination.map((element) => element.page)
  const $currentPageIndex = $pagination.map((element) => element.page - 1)
  const $limit = $pagination.map((element) => element.limit)
  const $totalPages = $pagination.map((element) => element.totalPages)
  const $total = $pagination.map((element) => element.total)
  const $isLast = $pagination.map((element) => element.isLast)
  const $params = $pagination.map((element) => element.params)
  const $searchValueOrigin = $pagination.map(
    (element) => element.searchValueOrigin,
  )
  const $searchValueDebounced = $pagination.map(
    (element) => element.searchValueDebounced,
  )

  const api = createApi($pagination, {
    nextPage: (state) => {
      if (state.page + 1 > state.totalPages) return state
      return { ...state, page: state.page + 1 }
    },
    prevPage: (state) => {
      if (state.page - 1 < 1) return state
      return { ...state, page: state.page - 1 }
    },
    setPage: (state, page) => ({ ...state, page }),
    setPageByIndex: (state, page) => ({
      ...state,
      page: page + 1,
    }),
    setLimit: (state, limit) => ({ ...state, limit, page: 1 }),
    setLast: (state, isLast) => ({ ...state, isLast }),
    resetPage: (state) => ({ ...state, page: 1, totalPages: 0 }),
    onChangeOrigin: (state, value) => ({
      ...state,
      searchValueOrigin: value,
      page: 1,
    }),
    onChangeDebounce: (state, value) => ({
      ...state,
      searchValueDebounced: value,
      page: 1,
    }),
    setParams: (state, newParam) => ({
      ...state,
      params: { ...state.params, ...newParam },
      page: 1,
    }),
  })

  let dynamicDefaultParams = {}

  const targetOnChange = debounce({ source: api.onChangeOrigin, timeout: 200 })

  for (const [key, value] of Object.entries(defaultParams)) {
    if (is.store(value)) {
      dynamicDefaultParams[key] = value
      // update stores of default params will change current state of params
      sample({
        source: value,
        fn: (string) => ({ [key]: string }),
        target: api.setParams,
      })
    }
  }

  dynamicDefaultParams = combine(dynamicDefaultParams)

  $pagination.on(
    sample({
      clock: resetParams, // event
      source: dynamicDefaultParams,
    }),
    (state, params) => ({
      ...state,
      page: 1,
      params: { ...defaultParamsOrigin, ...params },
    }),
  )

  sample({
    source: targetOnChange,
    target: api.onChangeDebounce,
  })

  sample({
    source: $pagination,
    fn: (state) => ({
      search: state.searchValueOrigin || undefined,
      limit: state.limit,
      page: state.page,
      ...Object.entries(state.params).reduce(
        (acc, [key, value]) => ({ ...acc, [key]: value || undefined }),
        {},
      ),
    }),
    target: effect,
  })

  $pagination
    .on(effect.doneData, (state, { data }) => ({
      ...state,
      isLast: data.isLast,
      totalPages: data.totalPages,
      total: data.total,
    }))
    .reset(reset)

  return {
    ...api,
    $pagination,
    $limit,
    $total,
    $currentPage,
    $currentPageIndex,
    $totalPages,
    $isLast,
    $searchValue: $searchValueOrigin,
    $params,
    reset,
    onChangeSearch: api.onChangeOrigin,
    resetParams,
  }
}

const getInitValueArg = (arg: Store<string> | string | string[]) => {
  return is.store(arg) ? arg.getState() || '' : arg
}
