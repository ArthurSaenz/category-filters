import { render, unmountComponentAtNode } from 'react-dom'
import { MemoryRouter } from 'react-router-dom'

import { Application } from './application'

describe('<App />', () => {
  test('renders without exploding', () => {
    const div = document.createElement('div')
    render(
      <MemoryRouter>
        <Application />
      </MemoryRouter>,
      div,
    )
    unmountComponentAtNode(div)
  })
})
