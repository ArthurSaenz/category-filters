import { createElement } from 'react'
import { render } from 'react-dom'

import { initLogger } from 'lib/logger'

import { Application } from './application'

render(createElement(Application), document.querySelector('#root'))

initLogger()
