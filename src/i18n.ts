import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import { initReactI18next } from 'react-i18next'
import LanguageDetector from 'i18next-browser-languagedetector'

i18n
  // load translation using xhr -> see /public/locales
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'ru',
    debug: false,
    whitelist: ['en', 'ru'],
    load: 'languageOnly',

    ns: ['common', 'join'],

    defaultNS: 'common',

    backend: {
      loadPath: '/static/locales/{{lng}}/{{ns}}.json',
    },
  })

export { i18n }
