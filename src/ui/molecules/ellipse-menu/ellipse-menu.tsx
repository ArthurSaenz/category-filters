/* eslint-disable react/prop-types */
import styled from 'styled-components'
import { MenuItem, MenuList } from '@material-ui/core'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import IconButton from '@material-ui/core/IconButton'

import { Box } from '../../atoms'
import { DropdownClean } from '../dropdown'

export const EllipseMenu = ({ actionsOption, isDisabled, id, ...rest }) => (
  <Box display="flex" alignItems="center" justifyContent="flex-end" px={1}>
    <DropdownClean
      header={({ handleClick, handleClose, open }) => (
        <IconButton
          onClick={(e) =>
            !isDisabled && (open ? handleClose() : handleClick(e))
          }
          aria-label="menu"
        >
          <MoreHorizIcon />
        </IconButton>
      )}
      inner={({ handleClose }) => (
        <StyledMenuList id={`ellipse-menu-${id}`}>
          {actionsOption.map(
            ({ label, handler, disabled, closeByDefault = true }, idx) => (
              <StyledMenuItem
                disabled={disabled}
                // eslint-disable-next-line react/no-array-index-key
                key={`${id}-${idx}`}
                onClick={(event) => {
                  if (closeByDefault) {
                    handleClose()
                  }
                  handler(id, { ...rest, event })
                }}
              >
                {label}
              </StyledMenuItem>
            ),
          )}
        </StyledMenuList>
      )}
    />
  </Box>
)

const StyledMenuList = styled(MenuList)`
  &.MuiList-padding {
    padding: 0;
  }
`

const StyledMenuItem = styled(MenuItem)`
  &.MuiMenuItem-root {
    padding-top: 1rem;
    padding-bottom: 1rem;
  }
`
