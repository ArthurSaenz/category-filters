import { useState } from 'react'
import { Popover } from '@material-ui/core'

// eslint-disable-next-line react/prop-types
export const DropdownClean = ({ header, inner }) => {
  const [anchorElement, setAnchorElement] = useState(null)

  const handleClick = (event) => {
    setAnchorElement(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorElement(null)
  }

  const open = Boolean(anchorElement)
  const id = open ? 'simple-popover' : undefined

  return (
    <div>
      {header({ handleClick, handleClose, open })}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorElement}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        {inner({ handleClose })}
      </Popover>
    </div>
  )
}
