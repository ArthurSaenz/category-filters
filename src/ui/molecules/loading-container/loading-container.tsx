import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'
import styled from 'styled-components'

export const LoadingContainer: React.FC<{ isLoading: boolean }> = ({
  isLoading,
}) => {
  return (
    <StyledBackDrop open={isLoading}>
      <CircularProgress color="inherit" />
    </StyledBackDrop>
  )
}

const StyledBackDrop = styled(Backdrop)`
  &.MuiBackdrop-root {
    z-index: ${(p) => p.theme.zIndex.drawer + 1};
    color: var(--white);
  }
`
