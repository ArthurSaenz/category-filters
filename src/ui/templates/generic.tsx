import React from 'react'
import styled from 'styled-components'

interface GenericTemplateProps {
  header: React.ReactNode
  sidebar: React.ReactNode
}

export const GenericTemplate: React.FC<GenericTemplateProps> = ({
  header,
  sidebar,
  children,
}) => (
  <MainContainer>
    {header}
    <Container>
      <Sidebar>{sidebar}</Sidebar>
      <Main>{children}</Main>
    </Container>
  </MainContainer>
)

const MainContainer = styled.div`
  height: 100vh;
  overflow: hidden;
`

const Container = styled.div`
  display: flex;
  height: calc(100vh - var(--header-height-desktop));

  ${(p) => p.theme.breakpoints.down('sm')} {
    height: calc(100vh - var(--header-height-mobile));
  }
`

const Sidebar = styled.aside`
  width: 200px;
`

const Main = styled.main`
  width: 100%;
  overflow: auto;

  ::-webkit-scrollbar {
    width: 11px;
  }

  scrollbar-width: thin;
  scrollbar-color: var(--thumb-background) var(--scrollbar-background);

  ::-webkit-scrollbar-track {
    background: var(--scrollbar-background);
  }

  ::-webkit-scrollbar-thumb {
    background-color: var(--thumb-background);
    border: 3px solid var(--scrollbar-background);
    border-radius: 6px;
  }
`
