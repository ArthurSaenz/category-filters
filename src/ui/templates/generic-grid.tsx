import styled from 'styled-components'

interface GenericTemplateProps {
  header: React.ReactNode
  sidebar: React.ReactNode
}

export const GenericTemplate: React.FC<GenericTemplateProps> = ({
  header,
  sidebar,
  children,
}) => (
  <MainContainer>
    <Header>{header}</Header>
    <Sidebar>{sidebar}</Sidebar>
    <Main>{children}</Main>
  </MainContainer>
)

const MainContainer = styled.div`
  display: grid;
  grid-template-areas:
    'header header'
    'sidebar main';
  grid-template-rows:
    minmax(50px, auto)
    1fr;
  grid-template-columns: 200px 1fr;
  height: 100vh;
  overflow: hidden;
`

const Header = styled.header`
  position: sticky;
  top: 0;
  z-index: 1;
  grid-area: header;
`
const Sidebar = styled.aside`
  grid-row: sidebar;
`

const Main = styled.main`
  grid-column: main;
  overflow: auto;

  ::-webkit-scrollbar {
    width: 11px;
  }

  scrollbar-width: thin;
  scrollbar-color: var(--thumb-background) var(--scrollbar-background);

  ::-webkit-scrollbar-track {
    background: var(--scrollbar-background);
  }

  ::-webkit-scrollbar-thumb {
    background-color: var(--thumb-background);
    border: 3px solid var(--scrollbar-background);
    border-radius: 6px;
  }
`
