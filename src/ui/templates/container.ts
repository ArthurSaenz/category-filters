import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  max-width: var(--container-width-desktop);
  margin: 0 auto;
  background: var(--body-bg);

  ${(p) => p.theme.breakpoints.down('md')} {
    max-width: var(--container-width-mobile);
  }
`
