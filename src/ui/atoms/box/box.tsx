/* eslint-disable @typescript-eslint/no-explicit-any */
import styled, { css, ThemedStyledProps } from 'styled-components'
import { Box as BoxMui, BoxProps } from '@material-ui/core'

interface Props extends BoxProps {
  gap?: number
}

export const Box = styled(BoxMui)`
  ${(
    p: ThemedStyledProps<
      | React.PropsWithChildren<Props>
      | (Props & React.RefAttributes<React.Component<Props, any, any>>),
      any
    >,
  ) =>
    p.gap && p.display === 'flex'
      ? css`
          &.MuiBox-root > *:not(:first-child) {
            margin-left: ${p.theme.spacing(p.gap || 0)}px;
          }
        `
      : css`
          &.MuiBox-root > *:not(:first-child) {
            margin-top: ${p.theme.spacing(p.gap || 0)}px;
          }
        `}
`
