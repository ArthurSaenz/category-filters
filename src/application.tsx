import { Suspense, useEffect } from 'react'
import { Router } from 'react-router'

import { history } from 'features/navigation'
import { readyToLoadSession } from 'features/session'

import { ThemeProvider } from '@arters/ui/themes'

import { Pages } from './pages'
import { Globals } from './globals'

import './i18n'

export const Application: React.FC = () => (
  <>
    <Suspense fallback={null}>
      <Router history={history}>
        <ThemeProvider>
          <Internal />
        </ThemeProvider>
      </Router>
    </Suspense>
  </>
)

const Internal: React.FC = () => {
  useEffect(() => readyToLoadSession(), [])

  return (
    <>
      <Globals />
      <Pages />
    </>
  )
}
